import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularAdressDataComponent } from './register-formular-adress-data.component';

describe('RegisterFormularAdressDataComponent', () => {
  let component: RegisterFormularAdressDataComponent;
  let fixture: ComponentFixture<RegisterFormularAdressDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularAdressDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularAdressDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
