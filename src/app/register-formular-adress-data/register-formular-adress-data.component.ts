import {Component, Host, OnInit} from '@angular/core';
import {RegisterFormularComponent} from '../register-formular/register-formular.component';
import {RegisterFormularBasicDataComponent} from '../register-formular-basic-data/register-formular-basic-data.component';

@Component({
  selector: 'app-register-formular-adress-data',
  templateUrl: './register-formular-adress-data.component.html',
  styleUrls: ['./register-formular-adress-data.component.css']
})
export class RegisterFormularAdressDataComponent extends RegisterFormularBasicDataComponent {

}
