import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegiserFormularTokenAuthenticationComponent } from './regiser-formular-token-authentication.component';

describe('RegiserFormularTokenAuthenticationComponent', () => {
  let component: RegiserFormularTokenAuthenticationComponent;
  let fixture: ComponentFixture<RegiserFormularTokenAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegiserFormularTokenAuthenticationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegiserFormularTokenAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
