import { Component, OnInit } from '@angular/core';
import {RegisterFormularBasicDataComponent} from '../register-formular-basic-data/register-formular-basic-data.component';

@Component({
  selector: 'app-regiser-formular-token-authentication',
  templateUrl: './regiser-formular-token-authentication.component.html',
  styleUrls: ['./regiser-formular-token-authentication.component.css']
})
export class RegiserFormularTokenAuthenticationComponent extends RegisterFormularBasicDataComponent implements OnInit {

  ngOnInit() {
    const self = this;
    setInterval(function () {
      if ( self.parent.data.token.length > 7) {
        self.parent.validateToken();
      }
    }, 1000);
  }

}
