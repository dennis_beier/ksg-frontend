import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RegisterFormularComponent } from './register-formular/register-formular.component';
import { RegisterFormularBasicDataComponent } from './register-formular-basic-data/register-formular-basic-data.component';
import { RegisterFormularAdressDataComponent } from './register-formular-adress-data/register-formular-adress-data.component';
import { RegisterFormularMemberFeeComponent } from './register-formular-member-fee/register-formular-member-fee.component';
import { RegisterFormularAdditionalInformationComponent } from './register-formular-additional-information/register-formular-additional-information.component';
import {HttpClientModule} from '@angular/common/http';
import { RegiserFormularTokenAuthenticationComponent } from './regiser-formular-token-authentication/regiser-formular-token-authentication.component';
import { RegisterFormularGratificationComponent } from './register-formular-gratification/register-formular-gratification.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterFormularComponent,
    RegisterFormularBasicDataComponent,
    RegisterFormularAdressDataComponent,
    RegisterFormularMemberFeeComponent,
    RegisterFormularAdditionalInformationComponent,
    RegiserFormularTokenAuthenticationComponent,
    RegisterFormularGratificationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
