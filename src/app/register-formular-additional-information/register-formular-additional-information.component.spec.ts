import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularAdditionalInformationComponent } from './register-formular-additional-information.component';

describe('RegisterFormularAdditionalInformationComponent', () => {
  let component: RegisterFormularAdditionalInformationComponent;
  let fixture: ComponentFixture<RegisterFormularAdditionalInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularAdditionalInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularAdditionalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
