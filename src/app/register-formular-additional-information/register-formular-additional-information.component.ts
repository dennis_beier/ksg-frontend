import {Component, Host, OnInit} from '@angular/core';
import {RegisterFormularBasicDataComponent} from '../register-formular-basic-data/register-formular-basic-data.component';

@Component({
  selector: 'app-register-formular-additional-information',
  templateUrl: './register-formular-additional-information.component.html',
  styleUrls: ['./register-formular-additional-information.component.css']
})
export class RegisterFormularAdditionalInformationComponent extends RegisterFormularBasicDataComponent {

}
