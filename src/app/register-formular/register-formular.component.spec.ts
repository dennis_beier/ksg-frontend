import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularComponent } from './register-formular.component';

describe('RegisterFormularComponent', () => {
  let component: RegisterFormularComponent;
  let fixture: ComponentFixture<RegisterFormularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
