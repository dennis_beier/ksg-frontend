import {AfterViewInit, Component, Injectable, OnChanges, OnInit} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@Component({
  selector: 'app-register-formular',
  templateUrl: './register-formular.component.html',
  styleUrls: ['./register-formular.component.css']
})
@Injectable()
export class RegisterFormularComponent implements OnInit {

  currentStepNumber: number;
  data = {
    token: '',
    tokenvalid: false,
    basic: {
      firstname: '',
      lastname: '',
      email: '',
      dataprotect1: 0,
      policyagreement: 0
    },
    contact: {
      street: '',
      number: '',
      zipcode: '',
      city: '',
      amandment: '',
      country: ''
    },
    birthdates: {
      country: '',
      citizenship: '',
      day: 1,
      month: 1,
      year: 1970,
      gender: '',
    },
    payment: {
      memberfee: 3.50,
      reduced: 0,
      rhythm: 0,
      iban: '',
      bic: '',
      accountOwner: ''
    },
    optional: {
      employmentrelationship: '',
      profession: '',
      tradeunion: '',
      formerparties: '',
      recruiter: ''
    },
    phone: {
      private: '',
      buisness: '',
      mobile: ''
    }
  };
  url =  'http://api.kurtschumacher.org';
  duplicateEmail = false;
  constructor(private http: HttpClient) {
    this.currentStepNumber = 0;

  }

  forward() {

    const form: HTMLFormElement = <HTMLFormElement>document.getElementById('mf-form');
    if (form.checkValidity() && !this.duplicateEmail) {
      this.currentStepNumber = this.currentStepNumber + 1 % 5;
    } else {
      console.log('invalid');
      form.reportValidity();
    }
  }

  goto(step: number) {
    this.currentStepNumber = step < this.currentStepNumber ?  step : this.currentStepNumber;
  }

  ngOnInit() {
  }

  isPreview() {
    return this.currentStepNumber === 5;
  }

  birthDate() {
    return '' + this.data.birthdates.day + '.' + this.data.birthdates.month + '.' + this.data.birthdates.year;
  }
  back() {
    this.currentStepNumber--;
  }

  send() {
    console.log('send');
    const self = this;
    this.http.post(this.url + '/member', JSON.stringify(this.data)).subscribe(function () {
      console.log('registration complete');
      self.forward();
    }, function () {
      alert('Irgendwas hat nicht funktioniert.');
    });
  }
  validateEmail() {
    const self = this;
    const promise = this.http.post(this.url + '/validate', JSON.stringify(this.data)).subscribe(function () {
        self.duplicateEmail = false;
      }, function (response) {
        self.duplicateEmail = true;
      }
    );
  }
  validateToken() {
    if (this.currentStepNumber === 0 ) {
      const self = this;
      const promise = this.http.post(this.url + '/validateToken', JSON.stringify(this.data)).subscribe(function () {
          self.data.tokenvalid = true;
          self.forward();
        }, function (response) {
          self.data.tokenvalid = false;
        }
      );
    }
  }

}
