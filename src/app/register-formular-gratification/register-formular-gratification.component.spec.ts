import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularGratificationComponent } from './register-formular-gratification.component';

describe('RegisterFormularGratificationComponent', () => {
  let component: RegisterFormularGratificationComponent;
  let fixture: ComponentFixture<RegisterFormularGratificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularGratificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularGratificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
