import {Component, Host, OnInit} from '@angular/core';
import {RegisterFormularBasicDataComponent} from '../register-formular-basic-data/register-formular-basic-data.component';

@Component({
  selector: 'app-register-formular-member-fee',
  templateUrl: './register-formular-member-fee.component.html',
  styleUrls: ['./register-formular-member-fee.component.css']
})
export class RegisterFormularMemberFeeComponent extends RegisterFormularBasicDataComponent {

}
