import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularMemberFeeComponent } from './register-formular-member-fee.component';

describe('RegisterFormularMemberFeeComponent', () => {
  let component: RegisterFormularMemberFeeComponent;
  let fixture: ComponentFixture<RegisterFormularMemberFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularMemberFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularMemberFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
