import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFormularBasicDataComponent } from './register-formular-basic-data.component';

describe('RegisterFormularBasicDataComponent', () => {
  let component: RegisterFormularBasicDataComponent;
  let fixture: ComponentFixture<RegisterFormularBasicDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterFormularBasicDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFormularBasicDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
