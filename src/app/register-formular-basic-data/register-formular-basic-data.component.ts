import {Component, Host, OnInit} from '@angular/core';
import {RegisterFormularComponent} from '../register-formular/register-formular.component';

@Component({
  selector: 'app-register-formular-basic-data',
  templateUrl: './register-formular-basic-data.component.html',
  styleUrls: ['./register-formular-basic-data.component.css']
})
export class RegisterFormularBasicDataComponent implements OnInit {

  parent: RegisterFormularComponent;
  constructor(@Host() parent: RegisterFormularComponent) {
    this.parent = parent;
  }

  ngOnInit() {
  }

  forward() {
    this.parent.forward();
  }

  validateEmail() {
    this.parent.validateEmail();
  }
}
